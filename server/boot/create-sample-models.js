var fs = require('fs');
var Promise = require('bluebird');

module.exports = function (app, cb) {
    var tables = ['employee', 'project', 'title', 'user'];
    var ds = app.dataSources.db;
    var dirname = './fixtures/development/';

    console.log('Creating sample models... wait a moment...');

    if (ds.connected) {
        ds.automigrate(tables, automigrate);
    } else {
        ds.once('connected', function () {
            ds.automigrate(tables, automigrate);
        });
    }

    function automigrate(err) {
        if (err) throw err;

        readDirectory(dirname)
            .then(function (filenames) {
                readFiles(dirname, filenames)
                    .then(function (models) {
                        var createModelPromises = [];

                        models.forEach(function (model) {
                            createModelPromises.push(createModel(app, model.name, model.content));
                        });

                        Promise
                            .all(createModelPromises)
                            .then(function () {
                                console.log('...all sample models has been read from fixtures and loaded into database.\n');

                                process.nextTick(cb);
                            })
                            .catch(function (error) {
                                throw error;
                            });

                    })
                    .catch(function (error) {
                        throw error;
                    });
            })
            .catch(function (error) {
                throw error;
            });
    }
};

function readDirectory(dirname) {
    var readdirAsync = Promise.promisify(fs.readdir);

    return readdirAsync(dirname);
}

function readFiles(dirname, filenames) {
    var promises = [];

    filenames.forEach(function (filename) {
        promises.push(readFile(dirname, filename));
    });

    return Promise.all(promises);
}

function readFile(dirname, filename) {
    var readFileAsync = Promise.promisify(fs.readFile);

    return readFileAsync(dirname + filename, 'utf-8')
        .then(function (content) {
            var modelName = filename.split('.')[0] || filename;

            return {name: modelName, content: content};
        });
}

function createModel(app, modelName, content) {
    return new Promise(function (resolve, reject) {
        app.models[modelName].create(JSON.parse(content), function (err, createdObjects) {
            if (err) {
                reject(err);
            }

            if (process.env.NODE_ENV !== 'test') {
                console.log('\n' + modelName + ' -> created: \n', createdObjects);
            }

            resolve();
        });
    });
}
