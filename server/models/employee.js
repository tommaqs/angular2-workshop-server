module.exports = function(Employee) {

  Employee.observe('before save', function employeeBeforeSave(ctx, next) {
    var randomTimeout = 1000 + Math.random()*1000;

    setTimeout(function () {
      next();
    }, randomTimeout);
  });
};
